#pragma once

#include <iostream>
#include <cassert>
#include <functional>
#include <limits>
#include <algorithm>
#include <ios>
#include <string>

class Aeroflot
{
	/**
     * @brief ������� ����� � �������� ����������� �����.
     *  ��������� ������� ��� ��������� ����� �����,
     *  � ������ ���� ����� ������ ������� �����, �������
     *  ��������� � �������� ��������� ����.
     *
     * ������ ����� ����� �������:
     * input_check("\nEnter size of array /> ", size, { [](const int& size)->const bool& {return size <= 0; } });
     *
     * @tparam Arg ����������� ��� �����, �� ��������������
     *  ������������ ������.
     * @tparam T ��������� ��� ������.
     * @tparam function ������ ���������, ��������� ��� �������.
     * @param message ��������� � ������ ������
     */
	template <typename T, typename Arg>
	void input_check(const T& message, Arg& arg,
		const std::function<const bool&(const Arg& var)>& lambda = NULL)
	{
		static_assert(std::is_same<T, char>::value ||
			std::is_array<T>::value ||
			std::is_same<T, std::string>::value,
			"For message, use string types!");

		do
		{
			std::cout << message;

			while (!(std::cin >> arg))
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // ������� ������ �����

				std::cout << "Error: " << message;
			}

		} while (lambda != NULL && lambda(arg) == true);

		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // ������� ������ �����
	}

	// ����� � �����
	struct Flight
	{
		std::string destination_point; // ����� ����������
		std::string type_of_tar; // ��� �������
		size_t flight_number; // ����� �����
	} *flight;

	const size_t _size;

public:
	Aeroflot(const int& _size);
	Aeroflot(const Aeroflot &ae);
	~Aeroflot();

	void input();
	void print();
	// ��������� �� ����� ������ ����������� � ������ �����, 
	// �� �������������� ���������, ��� ����� ������� � ���������
	void find(const std::string& type);
};