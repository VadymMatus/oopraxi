#include "pch.h"
#include "Aeroflot.h"

Aeroflot::Aeroflot(const int& _size)
	:_size(_size)
{
	assert(_size > 0 && "Size array not be, lass or equel zerro!");

	flight = new Flight[_size];
}

Aeroflot::Aeroflot(const Aeroflot &ae)
	:_size(ae._size)
{

	for (register size_t i(0); i < ae._size; i++)
	{
		flight[i] = ae.flight[i];
	}
}


Aeroflot::~Aeroflot()
{
	assert(flight != NULL);

	delete[] flight;
}


void Aeroflot::input()
{
	// ����
	for (register int i(0); i < _size; i++)
	{
		std::cout << "Enter destination point/> ";
		getline(std::cin, flight[i].destination_point);

		std::cout << "Enter aircraft type/> ";
		getline(std::cin, flight[i].type_of_tar);

		input_check("Enter flight number/> ", flight[i].flight_number, { [](const int& var) {return var < 0; } });
	}

	// ����������
	for (register int i(0); i < _size - 1; i++)
	{
		if (flight[i].destination_point > flight[i + 1].destination_point)
		{
			std::swap(flight[i], flight[i + 1]);
			i = -1;
		}
	}
}


void Aeroflot::print()
{
	for (register int i(0); i < _size; i++)
		std::cout << "Destination point => " << flight[i].destination_point << std::endl
			<< "\tAircraft type => " << flight[i].type_of_tar << std::endl
			<< "\tFlight number => " << flight[i].flight_number << std::endl << std::endl;
}



// ��������� �� ����� ������ ����������� � ������ �����, 
// �� �������������� ���������, ��� ����� ������� � ���������
void Aeroflot::find(const std::string& type)
{
	bool is_type_exist(false);

	for (register int i(0); i < _size; i++)
		if (flight[i].type_of_tar.compare(type)==0)
		{
			std::cout << "Destination point => " << flight[i].destination_point << std::endl
				<< "\tAircraft type => " << flight[i].type_of_tar << std::endl
				<< "\tFlight number => " << flight[i].flight_number << std::endl << std::endl;

			is_type_exist = true;
		}

	if (is_type_exist == false)
		std::cout << "Aircraft type \"" << type << "\" not found!" << std::endl;
}
