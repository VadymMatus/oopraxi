#pragma once

//#define NGEBUG

#include <assert.h>
#include <iostream>
#include <iomanip>

class Matrix
{
public:
	Matrix(const size_t& _count_row, const size_t& _count_col);
	~Matrix();

	// Set matrix[][] value.
	void set(const size_t& x, const size_t& y, const int& value);

	// Print the matrix to console.
	void print();
	
	// Get matrix[][] element.
	int& get(const size_t& x, const size_t&  y);

private:
	const size_t _count_col;
	const size_t _count_row;
	int **_matrix;
public:
	// ����� ������� � ��������, �� ������ ���� � ���� �������� �������.
	const int run();
};
