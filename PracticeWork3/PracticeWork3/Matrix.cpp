#include "pch.h"
#include "Matrix.h"


Matrix::Matrix(const size_t& _count_row, const size_t& _count_col)
	:_count_row(_count_row), _count_col(_count_col)
{
	assert(_count_row > 0 && _count_col > 0 && "Size matrix not be less or equal zero!");

	// �������� �������.
	_matrix = new int*[_count_row];
	for (register size_t i(0); i < _count_row; i++)
		_matrix[i] = new int[_count_col];
}


Matrix::~Matrix()
{
	assert(_matrix != NULL);

	delete[] _matrix;
}


// Set matrix[][] value.
void Matrix::set(const size_t& x, const size_t& y, const int& value)
{
	assert((x >= 0 && x < _count_row) && (y >= 0 && y < _count_col) && "Indexs is not included in the boundary!");
	assert(_matrix != NULL);

	_matrix[x][y] = value;
}


// Print the matrix to console.
void Matrix::print()
{
	assert(_matrix != NULL);

	for (register size_t i(0); i < _count_row; i++, [](void) ->void {std::cout << std::endl; }())
		for (register size_t j(0); j < _count_col; j++)
			std::cout << std::setw(4) << _matrix[i][j];
}


// Get matrix[][] element.
int& Matrix::get(const size_t& x, const size_t&  y)
{
	assert((x >= 0 && x < _count_row) && (y >= 0 && y < _count_col) && "Indexs is not included in the boundary!");

	return _matrix[x][y];
}


// ����� ������� � ��������, �� ������ ���� � ���� �������� �������.
const int Matrix::run()
{
	for (register size_t i(0); i < _count_col; i++)
		for (register size_t j(0); j < _count_row; j++)
			if (_matrix[j][i] == 0)
				return i;

	return -1;
}
