﻿#include "pch.h"

using namespace std;

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * Пример ввода длины массива:
 * input_check("\nEnter size of array /> ", size, { [](const int& size)->const bool& {return size <= 0; } });
 *
 * @tparam Arg Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @tparam function Ламбда выражение, повторить при условии.
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename Arg>
void input_check(const T& message, Arg& arg,
	const function<const bool&(const Arg& var)>& lambda = NULL)
{
	static_assert(std::is_same<T, char>::value ||
		std::is_array<T>::value ||
		std::is_same<T, std::string>::value,
		"For message, use string types!");

	do
	{
		std::cout << message;

		while (!(std::cin >> arg))
		{
			std::cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода

			std::cout << "Error: " << message;
		}

	} while (lambda != NULL && lambda(arg) == true);

	std::cin.ignore(numeric_limits<streamsize>::max(), '\n'); // очистка буфера ввода
}

int main()
{
	Array example_array(6), *array = NULL;
	int size;
	int sum(-1);

	cout << "Example array:" << endl;
	for (size_t i = 0; i < 6; i++)
		example_array.set(i,-5 + rand()%10);

	example_array.print();
	sum = example_array.sum(-1);
	cout << "Max element => " << example_array.max() << endl;
	cout << "Sum => " << (sum == -1 ? "No even numbers!" : to_string(sum)) << endl << endl;

	input_check("Enter size of array /> ", size, { [](const int& var)->const bool& {return var <= 0; } });
	array = new Array(size);
	cout << endl <<"Fill array[" << size << "]:> " << endl;

	for (register size_t i(0); i < size; i++)
		input_check([=]()->string {return "Array[" + to_string(i+1) + "]/> "; }(), array->get(i));

	cout << "Your array:" << endl;
	array->print();
	sum = array->sum(-1);
	cout << "Max element => " << array->max() << endl;
	cout << "Sum => " << (sum == -1 ? "No even numbers!" : to_string(sum)) << endl;

	return EXIT_SUCCESS;
}
