#include "pch.h"
#include "Complex.h"

Complex::Complex() { re = 0; im = 0; }
Complex::Complex(double r, double i) { re = r; im = i; }

bool Complex:: operator > (Complex& com)
{
	return (this->re > com.re) || (this->re == com.re && this->im > com.im);
}

bool Complex ::operator < (Complex& com)
{
	return (this->re < com.re) || (this->re == com.re && this->im < com.im);
}

bool Complex::operator != (Complex& com)
{
	return (this->re != com.re || this->im != com.im);
}

bool Complex::operator==(Complex& com)
{
	return (this->re == com.re && this->im == com.im);
}

Complex Complex::operator*(Complex &com)
{
	return Complex(re * com.re - im * com.im, re * com.im + com.re * im);
}

Complex Complex::operator/(Complex &com)
{
	double k(re * re + com.im * com.im);
	return Complex((re * com.re + im * com.im) / k, (com.re * im - re * com.im) / k);
}

Complex& Complex::operator = (Complex com)
{
	this->re = com.re;
	this->im = com.im;
	return *this;
}

Complex Complex::operator+(Complex com)
{
	return Complex(this->re + com.re, this->im + com.im);
}

Complex Complex::operator-(Complex com)
{
	return Complex(this->re - com.re, this->im - com.im);
}

Complex& Complex::operator+=(Complex com)
{
	re += com.re;
	im += com.im;
	return *this;
}

Complex& Complex::operator-=(Complex com)
{
	re -= com.re;
	im -= com.im;
	return *this;
}

Complex& Complex::operator*=(Complex com)
{
	re *= com.re;
	im *= com.im;
	return *this;
}

Complex& Complex::operator/=(Complex com)
{
	re /= com.re;
	im /= com.im;
	return *this;
}
