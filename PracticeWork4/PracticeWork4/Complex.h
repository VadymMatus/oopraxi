#pragma once
class Complex
{
private:
	double re, im;
public:
	Complex();
	Complex(double r, double i);
	Complex& operator = (Complex);
	Complex operator + (Complex);
	Complex operator - (Complex);
	Complex operator * (Complex&);
	Complex operator / (Complex&);
	Complex& operator += (Complex);
	Complex& operator -= (Complex);
	Complex& operator *= (Complex);
	Complex& operator /= (Complex);
	friend std::istream& operator >> (std::istream&, Complex&);
	friend std::ostream& operator << (std::ostream&, const Complex&);
	bool operator == (Complex& com);
	bool operator != (Complex& com);
	bool operator > (Complex& com);
	bool operator < (Complex& com);
};

